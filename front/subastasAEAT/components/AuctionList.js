import React, { Component } from 'react';
import { FlatList, View } from 'react-native';

import AuctionListItem from '../components/AuctionListItem';


export default class AuctionList extends Component {
  render() {
    return (
      <View>
        <FlatList
          data={this.props.dataSource}
          renderItem={({item}) => <AuctionListItem element={item} onClick={() => this.props.onItemClick(item)}></AuctionListItem>}
          keyExtractor={({id}, index) => id}
        />
      </View>
    );
  }
}
