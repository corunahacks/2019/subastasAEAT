import React, { Component } from "react";
import { FlatList, View, ActivityIndicator, ScrollView } from "react-native";
import { Container, Header, Content, Tab, Tabs, TabHeading, Text, Badge } from 'native-base';

import InfoGeneral from "../components/card/InfoGeneral";
import Bien from "../components/card/Bien";
import AutoridadGestora from "../components/card/AutoridadGestora"
import AcreedorPrivilegiadoEspecial from "../components/card/AcreedorPrivilegiadoEspecial"
import AdministradorConcursal from "../components/card/AdministradorConcursal"

export default class Card extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (!!!this.props.dataSource) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    let numBienes= 0;
    numBienes = this.props.dataSource.bienes.length;
    return (
      <Container>
        <Tabs>
          <Tab heading={ <TabHeading>
            <Text>Bienes
            </Text>
            <Badge>
                <Text>{numBienes}</Text>
              </Badge>
          </TabHeading>}>
            <FlatList
              data={this.props.dataSource.bienes}
              renderItem={({item}) => <Bien bien={item}></Bien>}
              keyExtractor={({element}, index) => element.toString()}
            />
          </Tab>
          <Tab heading={ <TabHeading><Text>Más Información</Text></TabHeading>}>
            <ScrollView>
              <View>
                <InfoGeneral
                  title="Información General"
                  informacion_general={this.props.dataSource.informacion_general}
                />
              </View>
              <View>
                <AutoridadGestora
                  title="Autoridad Gestora"
                  autoridad_gestora={this.props.dataSource.autoridad_gestora}
                />
              </View>
              <View>
                <AcreedorPrivilegiadoEspecial
                  title="Acreedor Privilegiado Especial"
                  acreedor_privilegiado_especial={this.props.dataSource.acreedor_privilegiado_especial}
                />
              </View>
              <View>
                <AdministradorConcursal
                  title="Administrador Concursal"
                  administrador_concursal={this.props.dataSource.administrador_concursal}
                />
              </View>
            </ScrollView>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
