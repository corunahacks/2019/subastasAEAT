import React, { Component } from 'react';
import { AppRegistry, Image, SectionList, StyleSheet, View, Alert, TouchableOpacity } from 'react-native';
import { Container, Badge, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

export default class AuctionListItem extends Component {
  render() {
    const element = this.props.element;
    const {iconType, iconName} = this.getIcon(element);
    const goods = element.goods ? element.goods.length : 0;
    const price = element.price.toFixed(2).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " €";
    const endDate = element.dateEnd.slice(0,10).replace(/-/g,"/");
    return (
    <TouchableOpacity onPress={()=> {
      this.props.onClick(element);
      }
    }>
          <Card>
            <CardItem>
              <Left>
                <Icon type={iconType} name={ iconName } />
                <Body>
                  <Text>{ element.town } - { element.province }</Text>
                  <Text note>{endDate}</Text>
                </Body>
              </Left>
            </CardItem>
            {element.images && element.images.length > 0 &&
               <CardItem cardBody>
                <Image source={{uri: element.images[0].url}} style={{height: 250, width: null, flex: 1}}/>
              </CardItem>
            }
            <CardItem>
              <Left>
                <Button transparent>
                  <Badge primary><Text>{goods} { goods === 1 ? 'Bien' : 'Bienes' }</Text></Badge>
                </Button>
              </Left>
              <Body>
                <Button transparent success>
                  <Badge success><Text>{price}</Text></Badge>
                </Button>
              </Body>
            </CardItem>
          </Card>
    </TouchableOpacity>
    );
  }

  getIcon(element) {
    const firstGood = element.goods ? element.goods[0] : {}
    switch (firstGood.type) {
      case 'HOUSE':
        return {iconType: 'AntDesign', iconName: 'home'}
        break;
      case 'GARAGE':
        return {iconType: 'MaterialCommunityIcons', iconName: 'garage'};
      default:
        return {iconType: 'AntDesign', iconName: 'questioncircleo'}
    }
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   padding: 10,
   borderBottomColor: '#47315a',
   borderBottomWidth: 1,
  }
})
