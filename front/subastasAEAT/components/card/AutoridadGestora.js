import React, { Component } from "react";
import {
  AppRegistry,
  SectionList,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity
} from "react-native";

export default class AutoridadGestora extends Component{
	render(){
		return (
			<View style={styles.container}>

		        <Text h1>{this.props.title}</Text>
        		<Text />

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Código:</Text>
		          <Text> {this.props.autoridad_gestora.codigo}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Descripción:</Text>
		          <Text> {this.props.autoridad_gestora.descripcion}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Dirección:</Text>
		          <Text> {this.props.autoridad_gestora.direccion}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Teléfono:</Text>
		          <Text> {this.props.autoridad_gestora.telefono}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Fax:</Text>
		          <Text> {this.props.autoridad_gestora.fax}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Correo electrónico:</Text>
		          <Text> {this.props.autoridad_gestora.correo_electronico}</Text>
		        </Text>
	      	</View>
			);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    borderBottomColor: "#47315a",
    borderBottomWidth: 1
  }
});
