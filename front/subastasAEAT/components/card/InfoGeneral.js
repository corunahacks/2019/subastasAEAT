import React, { Component } from "react";
import {
  AppRegistry,
  SectionList,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity
} from "react-native";

export default class InfoGeneral extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text h1>{this.props.title}</Text>
        <Text />
        <Text>
          <Text style={{ fontWeight: "bold" }}>Identificador:</Text>
          <Text> {this.props.informacion_general.identificador}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Tipo de subasta:</Text>
          <Text> {this.props.informacion_general.tipo_de_subasta}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Fecha de inicio:</Text>
          <Text> {this.props.informacion_general.fecha_de_inicio}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Fecha de conclusión:</Text>
          <Text> {this.props.informacion_general.fecha_de_conclusion}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Cantidad reclamada:</Text>
          <Text> {this.props.informacion_general.cantidad_reclamada}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Lotes:</Text>
          <Text> {this.props.informacion_general.lotes}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Anuncio BOE:</Text>
          <Text> {this.props.informacion_general.anuncio_boe}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Valor subasta:</Text>
          <Text> {this.props.informacion_general.valor_subasta}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Tasación:</Text>
          <Text> {this.props.informacion_general.tasacion}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Puja mínima:</Text>
          <Text> {this.props.informacion_general.puja_minima}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Tramos entre pujas:</Text>
          <Text> {this.props.informacion_general.tramos_entre_pujas}</Text>
        </Text>
        <Text>
          <Text style={{ fontWeight: "bold" }}>Importe del depósito:</Text>
          <Text> {this.props.informacion_general.importe_del_deposito}</Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    borderBottomColor: "#47315a",
    borderBottomWidth: 1
  }
});
