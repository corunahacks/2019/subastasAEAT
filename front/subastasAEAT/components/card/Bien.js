import React, { Component } from "react";
import {
  AppRegistry,
  SectionList,
  StyleSheet,
  View,
  Image,
  Alert,
  TouchableOpacity
} from "react-native";
import { Container, Badge, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import ReadMore from 'react-native-read-more-text';


export default class Bien extends Component{
	render(){
		return (
			<View style={styles.container}>

        <Card>
          <CardItem header>
            <Text>{this.props.bien.tipo} ({this.props.bien.subtipo})</Text>
          </CardItem>
          <CardItem cardBody>
          {this.props.bien.datos.images && this.props.bien.datos.images.length > 0 &&
              <Image source={{uri: this.props.bien.datos.images[0].url}} style={{height: 250, width: null, flex: 1}}/>
          }
          </CardItem>
          <CardItem>
            <Left>
              <Body>
              <Text style={{ fontWeight: "bold" }}>Descripción:</Text>
                <ReadMore
                  numberOfLines={3}
                  renderTruncatedFooter={this._renderTruncatedFooter}
                  renderRevealedFooter={this._renderRevealedFooter}
                  >
                  <Text>
                    {this.props.bien.datos.descripcion}
                  </Text>
                </ReadMore>
              </Body>
            </Left>
          </CardItem>
          <CardItem header>
            <Text style={{ fontWeight: "bold" }}>{this.props.bien.datos.localidad} ({this.props.bien.datos.provincia})</Text>
          </CardItem>
          <CardItem>
            <Text>
              <Text>{this.props.bien.datos.direccion} {this.props.bien.datos.codigo_postal}</Text>
            </Text>
          </CardItem>

          <CardItem>
            <Body>
            <Text>
              <Text style={{ fontWeight: "bold" }}>IDUFIR:</Text>
              <Text> {this.props.bien.datos.idufir}</Text>
            </Text>

            <Text>
              <Text style={{ fontWeight: "bold" }}>Referencia catastral:</Text>
              <Text> {this.props.bien.datos.referencia_catastral}</Text>
            </Text>

            <Text>
              <Text style={{ fontWeight: "bold" }}>Situación posesoria:</Text>
              <Text> {this.props.bien.datos.situacion_posesoria}</Text>
            </Text>

            <Text>
              <Text style={{ fontWeight: "bold" }}>Visitable:</Text>
              <Text> {this.props.bien.datos.visitable}</Text>
            </Text>

            <Text>
              <Text style={{ fontWeight: "bold" }}>Cargas:</Text>
              <Text> {this.props.bien.datos.cargas}</Text>
            </Text>

            <Text>
              <Text style={{ fontWeight: "bold" }}>Inscripción registral:</Text>
              <Text> {this.props.bien.datos.inscripcion_registral}</Text>
            </Text>
            </Body>
          </CardItem>
        </Card>
	     </View>
			);
	}

    _renderTruncatedFooter = (handlePress) => {
    return (
      <Text onPress={handlePress} style={{ color: "blue" }}>
        Leer más...
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text onPress={handlePress} style={{ color: "blue" }}>
        Mostrar menos...
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    borderBottomColor: "#47315a",
  }
});
