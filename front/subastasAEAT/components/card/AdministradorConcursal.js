import React, { Component } from "react";
import {
  AppRegistry,
  SectionList,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity
} from "react-native";

export default class AdministradorConcursal extends Component{
	render(){
		return (
			<View style={styles.container}>

		        <Text h1>{this.props.title}</Text>
        		<Text />

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Nombre:</Text>
		          <Text> {this.props.administrador_concursal.nombre}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>NIF:</Text>
		          <Text> {this.props.administrador_concursal.nif}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Dirección:</Text>
		          <Text> {this.props.administrador_concursal.direccion}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Localidad:</Text>
		          <Text> {this.props.administrador_concursal.localidad}</Text>
		        </Text>

		        <Text>
		          <Text style={{ fontWeight: "bold" }}>Provincia:</Text>
		          <Text> {this.props.administrador_concursal.provincia}</Text>
		        </Text>
	      	</View>
			);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    borderBottomColor: "#47315a",
    borderBottomWidth: 1
  }
});
