# Front side


Installation.

Assuming that you have Node 10+ installed, you can use npm to install the Expo CLI command line utility:


1) Install and configure:

Install expo-cli
```
npm install -g expo-cli
```

Install yarn
```
npm install -g yarn
```


2) Install proyect dependencies

```
yarn install
```

To launch the app run:

```
yarn start
```

Enjoi :)
