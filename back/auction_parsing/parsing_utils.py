from bs4 import BeautifulSoup
import re

VIEW_IDS = {'Información General': 1,
            'Autoridad Gestora': 2,
            'Lotes': 3,
            'Administrador Concursal': 7,
            'Pujas': 8}

PATTERN_ITEM_VERSION = re.compile('ver=[1-9][1-9]?', re.IGNORECASE)


def get_type_of_information_available(soup: BeautifulSoup):
    items =[]
    for item in soup.findAll('a', {'href': re.compile(f'detalle*')}):
        item_id = int(PATTERN_ITEM_VERSION.findall(str(item))[0].split('=')[1])
        items.append((item_id, item.text))
    return items


def parse_auction_general_information(auction_raw_general_information: BeautifulSoup):
    return process_key_value_pairs(auction_raw_general_information)


def process_key_value_pairs(html_soup: BeautifulSoup) -> dict:
    data = dict()
    for key_value in html_soup.findAll('tr'):
        # get key
        keys = key_value.findAll('th')
        assert (len(keys) == 1)
        key = keys[0].text

        # get value
        values = key_value.findAll('td')
        assert (len(values) == 1)
        value = values[0].text.strip()

        data[key] = value
    return data


def parse_auction_assets(auction_raw_assets: BeautifulSoup) -> list:
    assets = []
    for asset_html in auction_raw_assets.findAll('div', {'class': 'bloqueSubastaBien'}):
        #     print(asset_tag)
        asset_dict = process_key_value_pairs(html_soup=asset_html)
        asset_category = asset_html.findAll('h4', {'class': 'legend1'})[0].text.split('-')[1].strip()
        #     print(asset_category)
        asset_dict['asset_category'] = asset_category
        assets.append(asset_dict)
    return assets


def parse_bids(auction_raw_assets: BeautifulSoup):
    return {}


def parse_management_authority(auction_raw_assets: BeautifulSoup):
    return {}


def parse_auction_parts(auction_raw_assets: BeautifulSoup):
    return {}


def parse_contest_administrator(auction_raw_assets: BeautifulSoup):
    return {}
