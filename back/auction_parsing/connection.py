import requests


def get_request(url):
    headers = {
        'content-type': "application/json;charset=utf-8",
    }
    response = requests.request("GET", url, headers=headers)
    return response.text


def get_auction_view(auction_id: str, view_id: int):
    url = f'https://subastas.boe.es/detalleSubasta.php?idSub={auction_id}&ver={view_id}'
    text = get_request(url=url)
    return text


def get_auction_general_information_view(auction_id: str):
    url = f'https://subastas.boe.es/detalleSubasta.php?idSub={auction_id}'
    text = get_request(url=url)
    return text
