from auction_parsing.connection import get_auction_general_information_view, get_auction_view
from bs4 import BeautifulSoup

from auction_parsing.parsing_utils import parse_auction_general_information, get_type_of_information_available, \
    parse_auction_assets, parse_bids, parse_management_authority, parse_auction_parts, parse_contest_administrator

data_type_parsers = {'Información general': parse_auction_general_information,
                     'Autoridad gestora': parse_management_authority,
                     'Bienes': parse_auction_assets,
                     'Lotes': parse_auction_parts,
                     'Pujas': parse_bids,
                     'Administrador concursal': parse_contest_administrator}


def parse_auction(auction_id):
    '''
    Main method to get detailed information for a given action
    :param auction_id: such as SUB-JC-2019-120764
    :return: a dictionary with all detailed data regarding such action
    '''
    # get general information view
    auction_data = dict()
    auction_raw_text = get_auction_general_information_view(auction_id=auction_id)
    auction_soup = BeautifulSoup(auction_raw_text, 'html.parser')

    information_available = get_type_of_information_available(soup=auction_soup)

    for version_id, data_type in information_available:
        auction_raw_text = get_auction_view(auction_id=auction_id, view_id=version_id)
        auction_soup = BeautifulSoup(auction_raw_text, 'html.parser')
        auction_data[data_type] = data_type_parsers[data_type](auction_soup)

    return auction_data




