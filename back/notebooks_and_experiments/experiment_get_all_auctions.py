from bs4 import BeautifulSoup
import json
from auction_parsing.controller import parse_auction
from auction_querying.search_auctions import search_all_auctions


def main():
    auctions = search_all_auctions()
    assert(len(auctions) == 97094)
    auction_data_json = json.dumps(auctions)
    print(auction_data_json)


if __name__ == '__main__':
    main()
