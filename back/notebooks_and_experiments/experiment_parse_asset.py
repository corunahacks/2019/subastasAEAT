from bs4 import BeautifulSoup
import json
from auction_parsing.controller import parse_auction


def main():
    auction_id = 'SUB-JC-2019-120764'
    auction_id = 'SUB-JV-2019-120422'

    auction_data = parse_auction(auction_id)
    auction_data_json = json.dumps(auction_data)
    print(auction_data_json)


if __name__ == '__main__':
    main()
