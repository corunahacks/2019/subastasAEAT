# Backend for subastasAEAT
This part is composed by a scrapper to get info from AEAT web, and an
REST API backend to serve data to the mobile app

## Scrapper
Scrapper is developed in Python 3 using the scrapping library BeautifulSoup.
Install dependencies using `pip` and run the script `experiment_get_all_auctions`
to get a json representation of all auctions found in AEAT web.

This scrapper will be run periodically in a cron script on the backend server to
populate database used by the REST API

## REST API
You can contribute with a json REST API to provide data to mobile apps
