from bs4 import BeautifulSoup

from auction_parsing.connection import get_request

URL_INITIAL_SEARCH = f'https://subastas.boe.es/subastas_ava.php?campo%5B0%5D=SUBASTA.ORIGEN&dato%5B0%5D=&campo%5B1%5D=SUBASTA.ESTADO&dato%5B1%5D=&campo%5B2%5D=BIEN.TIPO&dato%5B2%5D=&campo%5B4%5D=BIEN.DIRECCION&dato%5B4%5D=&campo%5B5%5D=BIEN.CODPOSTAL&dato%5B5%5D=&campo%5B6%5D=BIEN.LOCALIDAD&dato%5B6%5D=&campo%5B7%5D=BIEN.COD_PROVINCIA&dato%5B7%5D=&campo%5B8%5D=SUBASTA.POSTURA_MINIMA_MINIMA_LOTES&dato%5B8%5D=&campo%5B9%5D=SUBASTA.NUM_CUENTA_EXPEDIENTE_1&dato%5B9%5D=&campo%5B10%5D=SUBASTA.NUM_CUENTA_EXPEDIENTE_2&dato%5B10%5D=&campo%5B11%5D=SUBASTA.NUM_CUENTA_EXPEDIENTE_3&dato%5B11%5D=&campo%5B12%5D=SUBASTA.NUM_CUENTA_EXPEDIENTE_4&dato%5B12%5D=&campo%5B13%5D=SUBASTA.NUM_CUENTA_EXPEDIENTE_5&dato%5B13%5D=&campo%5B14%5D=SUBASTA.ID_SUBASTA_BUSCAR&dato%5B14%5D=&campo%5B15%5D=SUBASTA.FECHA_FIN_YMD&dato%5B15%5D%5B0%5D=&dato%5B15%5D%5B1%5D=&campo%5B16%5D=SUBASTA.FECHA_INICIO_YMD&dato%5B16%5D%5B0%5D=&dato%5B16%5D%5B1%5D=&page_hits=40&sort_field%5B0%5D=SUBASTA.FECHA_FIN_YMD&sort_order%5B0%5D=desc&sort_field%5B1%5D=SUBASTA.FECHA_FIN_YMD&sort_order%5B1%5D=asc&sort_field%5B2%5D=SUBASTA.HORA_FIN&sort_order%5B2%5D=asc&accion=Buscar'


def search_all_auctions():
    '''
    Main method to search for auctions.
    :return: list of dictionaries. Each dictionary contains basic auction data
    '''
    page_soup = get_search_page_soup(url_search=URL_INITIAL_SEARCH)

    auctions = get_auctions_in_page(soup=page_soup)
    next_page_url = get_next_page_url(current_page_soup=page_soup)
    while next_page_url is not None:

        page_soup = get_search_page_soup(url_search=next_page_url)
        next_page_url = get_next_page_url(current_page_soup=page_soup)

        auctions.extend(get_auctions_in_page(soup=page_soup))
    return auctions


def get_auctions_in_page(soup: BeautifulSoup):
    auctions = []
    for auction_html in soup.findAll('li', {'class': 'resultado-busqueda'}):
        #     print(auction_html)
        auction_properties = auction_html.findAll('p', {'class': 'epigrafeDpto'})
        auction_data = dict()
        auction_data['auction_id'] = auction_properties[0].text.strip().split(' ')[1].strip()

        if len(auction_properties) > 1:
            auction_data['auctioneer'] = auction_properties[1].text.strip()
        if len(auction_properties) > 2:
            auction_data['case'] = auction_properties[2].text.strip().split(' ')[1]
        if len(auction_properties) > 3:
            auction_data['auction_state'] = auction_properties[3].text.strip()

        auction_data['asset_description'] = auction_html.find('p', {'class': 'documento'}).text.strip()
        auctions.append(auction_data)
    return auctions


def get_search_page_soup(url_search):
    print(f'Searching: {url_search}')
    soup = BeautifulSoup(get_request(url=url_search), 'html.parser')
    return soup


def get_total_number_of_pages(soup: BeautifulSoup):
    n_max_pages = 1
    for page in soup.findAll('span', {'class': 'pagSigxxx'}):
        n_current = int(page.text)
        if n_current > n_max_pages:
            n_max_pages = n_current
    return n_max_pages


def get_next_page_url(current_page_soup: BeautifulSoup):
    for li_tag in current_page_soup.findAll('li'):
        #     print(li_tag)
        next_page_tags = li_tag.findAll('span', {'class': 'pagSig'})
        if len(next_page_tags) > 0:
            # we have a next page available. Then, we get the url
            href = li_tag.find('a').attrs['href']
            url_next_page = f'https://subastas.boe.es/{href}'
            return url_next_page
    return None





