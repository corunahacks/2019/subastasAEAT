subastasAEAT contributors
============================================

* **[Pedro Vale](https://gitlab.com/pedrovr-)**

  * Author and maintainer
  * Full stack developer, backend maintainer

* **[Pablo Garcia](https://gitlab.com/Pablogs96)**
  * Author and maintainer
  * React Native developer

* **[Borja Maceira](https://gitlab.com/Maceira)**

  * Author and maintainer
  * React Native developer

* **[Alberto Castelo]**

  * Author and maintainer
  * Python expert and backend maintainer

* **[Luis Sevillano]**
  * Author and maintainer
  * React Native developer
