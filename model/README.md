## Subastas list model
Object with the properties:
- items: array of items related with the auction

### Item model
Properties:
- id
- goods: array of goods related with the auction
- dateInit
- dateEnd
- price
- state: state of the auction. Valid values: NEAR_OPEN, ACTIVE, STOPPED, CANCELLED, RESOLVED, COMPLETED
- town
- province

#### Goods model
Properties:
- title
- description
- type: Valid values: HOUSE, GARAGE, STORAGE, SITE, RUSTIC_SITE, COMMERCIAL_OFFICE, INDUSTRIAL_UNIT, VEHICLE, SHIP, AIRCRAFT, JEWEL, INDUSTRIAL_MACHINE, MERCHANDISE, FURNITURE, FACILITY, TOOL, INDUSTRIAL_RIGHT, INTELECTUAL_RIGHT, TRANSFER_RIGHT, OTHER 
