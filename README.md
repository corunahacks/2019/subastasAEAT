# subastasAEAT
A Reac-Native App for tracking auctions from The Agencia Tributaria Española

Data extract for https://subastas.boe.es/


Proyect folders:


backend
---

Phyton scrapper.

Read documentation:

[Documentation](https://gitlab.com/corunahacks/2019/subastasAEAT/blob/master/back/README.md)


front
---

React native app.

![Image 1](https://gitlab.com/corunahacks/2019/subastasAEAT/raw/master/screenshots/2.jpg "Image 1")
![Image 2](https://gitlab.com/corunahacks/2019/subastasAEAT/raw/master/screenshots/3.jpg "Image 2")



[Documentation](https://gitlab.com/corunahacks/2019/subastasAEAT/blob/master/front/README.md)


model
---

Representation of all data structure used in the application.



Contributing:
---

Communty slack channel: https://corunadevelopers.slack.com/messages/CGSVB943A/

Contributors
[Contributors](https://gitlab.com/corunahacks/2019/subastasAEAT/blob/master/CONTRIBUTORS.md)


License
---

MIT
